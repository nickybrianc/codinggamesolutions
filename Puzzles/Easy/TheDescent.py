import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.


# game loop
while True:
    maxI = 0
    maxNum = 0
    for i in range(8):
        mountain_h = int(input())  # represents the height of one mountain, from 9 to 0.
        if mountain_h > maxNum :
            maxI = i
            maxNum = mountain_h
        elif mountain_h < maxNum :
            print("Something", file=sys.stderr)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    print(maxI)