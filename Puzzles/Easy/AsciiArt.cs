using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class AsciiLetter{
    public int width;
    public int height;
    public List<string> lines;
    public AsciiLetter(int w, int h){
        width = w; height = h; lines = new List<string>();
    }
    public void AddLine(string s){
        lines.Add(s);   
    }
    public static string FormWord(List<AsciiLetter> word){
        string ret = "";
        for(int i=0;i<word[0].height;i++){
            for(int j=0;j<word.Count();j++){
                ret = ret + word[j].lines[i];
            }
            ret = ret +"\n";
        }
        return ret;
    }
}
class Solution
{
    static void Main(string[] args)
    {
        int L = int.Parse(Console.ReadLine());
        int H = int.Parse(Console.ReadLine());
        string T = Console.ReadLine();
        AsciiLetter[] alphabet = new AsciiLetter[27];
        for (int i = 0; i < H; i++)
        {
            string ROW = Console.ReadLine();
        
            for(int j=0;j<27;j++){
                if(i==0)
                    alphabet[j] = new AsciiLetter(L,H);
                alphabet[j].AddLine(ROW.Substring(j*L,L));
            }
        }
        List<AsciiLetter> ret = new List<AsciiLetter>();
        for(int i=0;i<T.Length;i++){
            if(Char.IsLetter(T[i]))
                ret.Add(alphabet[(int)(Char.ToUpper(T[i])-'A')]); 
            else
                ret.Add(alphabet[26]);
        }

        Console.WriteLine(AsciiLetter.FormWord(ret));
    }
}