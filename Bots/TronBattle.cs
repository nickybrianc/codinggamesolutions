using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
class Point { public int X; public int Y; }
class CyclePoint : Point { public int ID; }
class Cycle
{
	public int x, y;
	public int ID;
	public Board Board;

	internal int[] dx = { -1, 0, 1, 0 };
	internal int[] dy = { 0, -1, 0, 1 };

	public virtual string Move()
	{
		for (int i = 0; i < dx.Length; i++)
		{
			if (Board.IsValid(x + dx[i], y + dy[i]))
			{
				return Move(i);
			}
		}
		return Move(99);
	}
	internal string Move(int i)
	{
		switch (i)
		{
			case 0:
				return "LEFT";
			case 1:
				return "UP";
			case 2:
				return "RIGHT";
			case 3:
				return "DOWN";
			default:
				return @"DOWN DOWN DOWN 31337 m0\/3!";
		}
	}
}
class BronzeCycle : Cycle
{
	public override string Move()
	{
		int max = -1;
		int move = 99;

		for (int i = 0; i < dx.Length; i++)
		{
		    int x1 = x+dx[i];
		    int y1 = y+dy[i];
			if (Board.IsValid(x1, y1) && !Board._grid[y1,x1])
			{
				int temp = Score(x + dx[i], y + dy[i]);

				if (temp > max)
				{
					max = temp;
					move = i;
					
				
				}
			}
		}

		return Move(move);
	}
	public virtual int Score(int x, int y)
	{
		int[,] voroniDiagram = new int[Board.MAX_X, Board.MAX_Y];
        
        if(Board._grid[y, x]){
            return 0;
        }
        
		//Initialize
		for (int i = 0; i < Board.MAX_X; i++)
		{
			for (int j = 0; j < Board.MAX_Y; j++)
			{
				if (Board._grid[j, i])
				{
					voroniDiagram[i, j] = -1;
				}
				else
				{
					voroniDiagram[i, j] = 5;
				}
			}
		}
		//Board.PrintGrid(voroniDiagram, Board.MAX_X, Board.MAX_Y);

		var q = new Queue<CyclePoint>();

		q.Enqueue(
			new CyclePoint()
			{
				X = x,
				Y = y,
				ID = ID,
			});
		voroniDiagram[x,y] = ID;
		//Player Points
		foreach (var c in Board.cycles)
		{
			if(c.ID != ID){
    			voroniDiagram[c.x, c.y] = c.ID;
    			//Console.Error.WriteLine("Player "+c.ID+" running. "+voroniDiagram[x,y]);
    			q.Enqueue(
    				new CyclePoint()
    				{
    					ID = c.ID,
    					X = c.x,
    					Y = c.y,
    				});
			}
		}
		Console.Error.WriteLine(x+","+y);
		//Board.PrintGrid(voroniDiagram, Board.MAX_X, Board.MAX_Y);
		while (q.Count() > 0)
		{
			var t = q.Dequeue();

			if (voroniDiagram[t.X, t.Y] == t.ID)
			{
				for (int i = 0; i < dx.Length; i++)
				{
					if (Board.IsValid(t.X + dx[i], t.Y + dy[i])
						&& voroniDiagram[t.X + dx[i], t.Y + dy[i]] == 5)
					{
						voroniDiagram[t.X + dx[i], t.Y + dy[i]] = t.ID;
						q.Enqueue(
							new CyclePoint()
							{
								X = t.X + dx[i],
								Y = t.Y + dy[i],
								ID = t.ID
							});
					}
				}
			}
		}
		//Board.PrintGrid(voroniDiagram, Board.MAX_X, Board.MAX_Y);
		int score = 0;
		for (int i = 0; i < Board.MAX_X; i++)
		{
			for (int j = 0; j < Board.MAX_Y; j++)
			{
				if (voroniDiagram[i, j] == ID)
				{
					score++;
				}
			}
		}
		Console.Error.WriteLine("Score of: " + score);
		return score;
	}
}
class Board
{
	public const int MAX_X = 30;
	public const int MAX_Y = 20;
	public bool[,] _grid;
	public List<Cycle> cycles;

	internal int MyId;
	internal int NumPlayers;

	public Board(int numPlayers, int myId)
	{
		_grid = new bool[MAX_Y, MAX_X];

		MyId = myId + 1;
		NumPlayers = numPlayers;
	}

	public virtual void GetCycles()
	{
		cycles = new List<Cycle>();

		GetCycles(cycles);

		//PrintGrid();
	}

	public virtual void GetCycles(List<Cycle> cycles)
	{
		string[] inputs;
		for (int i = 0; i < NumPlayers; i++)
		{
			inputs = Console.ReadLine().Split(' ');
			int X0 = int.Parse(inputs[0]); // starting X coordinate of lightcycle (or -1)
			int Y0 = int.Parse(inputs[1]); // starting Y coordinate of lightcycle (or -1)
			int X1 = int.Parse(inputs[2]); // starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
			int Y1 = int.Parse(inputs[3]); // starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)

			_grid[Y0, X0] = true;
			_grid[Y1, X1] = true;

			cycles.Add(
				new Cycle()
				{
					x = X1,
					y = Y1,
					Board = this,
					ID = i + 1,
				});
		}
	}

	public virtual void PrintGrid()
	{
		for (int i = 0; i < MAX_Y; i++)
		{
			Console.Error.Write("[");
			for (int j = 0; j < MAX_X; j++)
			{
				if (j != MAX_X - 1) Console.Error.Write((_grid[i, j] ? "1" : "0") + ",");
				else Console.Error.Write((_grid[i, j] ? "1" : "0"));
			}
			Console.Error.WriteLine("]");
		}
	}
	public static void PrintGrid(int[,] grid, int MAX_X, int MAX_Y)
	{
		for (int i = 0; i < MAX_Y; i++)
		{
			Console.Error.Write("[");
			for (int j = 0; j < MAX_X; j++)
			{
				if (j != MAX_X - 1) 
				    Console.Error.Write(
				        string.Format("{0,2},",grid[j, i]));
				else Console.Error.Write((grid[j, i]));
			}
			Console.Error.WriteLine("]");
		}
		Console.Error.WriteLine("===END===");

	}
	public virtual void Update(int N, int P)
	{
		NumPlayers = N;
		MyId = P + 1;
	}
	public virtual string GetMoveForMe()
	{
	    Console.Error.WriteLine("ID: "+MyId+" "+cycles.Count());
	    //cycles.ForEach(c => Console.Error.WriteLine(c.ID));
		return cycles.First(c => c.ID == MyId).Move();
	}
	public bool IsValid(int x, int y)
	{
		return y >= 0 && y < MAX_Y && x >= 0 && x < MAX_X && !_grid[y, x];
	}
}
class BronzeBoard : Board
{

	public BronzeBoard(int numPlayers, int myId) : base(numPlayers, myId) { }

	public override void GetCycles()
	{
		cycles = new List<Cycle>();

		GetCycles(cycles);
	}
	public override void GetCycles(List<Cycle> cycles)
	{
		string[] inputs;

		for (int i = 0; i < NumPlayers; i++)
		{
			var inline = Console.ReadLine();
			Console.Error.WriteLine(inline);
			inputs = inline.Split(' ');
			int X0 = int.Parse(inputs[0]); // starting X coordinate of lightcycle (or -1)
			int Y0 = int.Parse(inputs[1]); // starting Y coordinate of lightcycle (or -1)
			int X1 = int.Parse(inputs[2]); // starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
			int Y1 = int.Parse(inputs[3]); // starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)
            
            if(X0 != -1 && Y0 != -1){
			    _grid[Y0, X0] = true;
			    _grid[Y1, X1] = true;
    
    			cycles.Add(
    				new BronzeCycle()
    				{
    					x = X1,
    					y = Y1,
    					Board = this,
    					ID = i + 1,
    				});
                }
		}
	}
}
class SilverCycle : BronzeCycle{
	public override string Move()
	{
		int max = int.MinValue;
		int move = 99;

		for (int i = 0; i < dx.Length; i++)
		{
		    int x1 = x+dx[i];
		    int y1 = y+dy[i];
			if (Board.IsValid(x1, y1) && !Board._grid[y1,x1])
			{
				int temp = Score(x + dx[i], y + dy[i]);
				
				int tmax = int.MinValue;
				for(int j=0;j<dx.Length;j++){
				    int x2 = x1 + dx[j];
				    int y2 = y1 + dy[j];
				    if(Board.IsValid(x2,y2) && !Board._grid[y2,x2]){
				        int temp2 = Score(x2,y2);
				        if(temp2 > tmax){
				            tmax = temp2;
				        }
				    }
				}
				temp += tmax;

				if (temp > max)
				{
					max = temp;
					move = i;
				}
			}
			else{
			    Console.Error.WriteLine("Move "+i+": ("+x1+","+y1+")");
			    //Board.PrintGrid();
			}
		}

		return Move(move);
	}
	public override int Score(int x, int y)
	{
		int[,] voroniDiagram = new int[Board.MAX_X, Board.MAX_Y];
        
        if(Board._grid[y, x]){
            return 0;
        }
        
		//Initialize
		for (int i = 0; i < Board.MAX_X; i++)
		{
			for (int j = 0; j < Board.MAX_Y; j++)
			{
				if (Board._grid[j, i])
				{
					voroniDiagram[i, j] = -1;
				}
				else
				{
					voroniDiagram[i, j] = 5;
				}
			}
		}
		//Board.PrintGrid(voroniDiagram, Board.MAX_X, Board.MAX_Y);

		var q = new Queue<CyclePoint>();

		q.Enqueue(
			new CyclePoint()
			{
				X = x,
				Y = y,
				ID = ID,
			});
		voroniDiagram[x,y] = ID;
		//Player Points
		foreach (var c in Board.cycles)
		{
			if(c.ID != ID){
    			voroniDiagram[c.x, c.y] = c.ID;
    			//Console.Error.WriteLine("Player "+c.ID+" running. "+voroniDiagram[x,y]);
    			q.Enqueue(
    				new CyclePoint()
    				{
    					ID = c.ID,
    					X = c.x,
    					Y = c.y,
    				});
			}
		}
		Console.Error.WriteLine(x+","+y);
		//Board.PrintGrid(voroniDiagram, Board.MAX_X, Board.MAX_Y);
		while (q.Count() > 0)
		{
			var t = q.Dequeue();

			if (voroniDiagram[t.X, t.Y] == t.ID)
			{
				for (int i = 0; i < dx.Length; i++)
				{
					if (Board.IsValid(t.X + dx[i], t.Y + dy[i])
						&& voroniDiagram[t.X + dx[i], t.Y + dy[i]] == 5)
					{
						voroniDiagram[t.X + dx[i], t.Y + dy[i]] = t.ID;
						q.Enqueue(
							new CyclePoint()
							{
								X = t.X + dx[i],
								Y = t.Y + dy[i],
								ID = t.ID
							});
					}
				}
			}
		}
		//Board.PrintGrid(voroniDiagram, Board.MAX_X, Board.MAX_Y);
		int score = 0;
		for (int i = 0; i < Board.MAX_X; i++)
		{
			for (int j = 0; j < Board.MAX_Y; j++)
			{
				if (voroniDiagram[i, j] == ID)
				{
					score++;
				}
				else if (voroniDiagram[i,j] != ID){
				    //score--;
				}
			}
		}
		Console.Error.WriteLine("Score of: " + score);
		return score;
	}

}
class SilverBoard : BronzeBoard{
    public SilverBoard(int numPlayers, int myId) : base(numPlayers, myId) { }
    
	public override void GetCycles(List<Cycle> cycles)
	{
		string[] inputs;

		for (int i = 0; i < NumPlayers; i++)
		{
			var inline = Console.ReadLine();
			Console.Error.WriteLine(inline);
			inputs = inline.Split(' ');
			int X0 = int.Parse(inputs[0]); // starting X coordinate of lightcycle (or -1)
			int Y0 = int.Parse(inputs[1]); // starting Y coordinate of lightcycle (or -1)
			int X1 = int.Parse(inputs[2]); // starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
			int Y1 = int.Parse(inputs[3]); // starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)
            
            if(X0 != -1 && Y0 != -1)
            {
                _grid[Y0, X0] = true;
                _grid[Y1, X1] = true;
    
                cycles.Add(
                    new SilverCycle()
                    {
                        x = X1,
                        y = Y1,
                        Board = this,
                        ID = i + 1,
                    });
            }
		}
	}
}
class Player
{
	static void Main(string[] args)
	{
		string[] inputs;
		SilverBoard b = null;
		// game loop
		while (true)
		{
			string inline = Console.ReadLine();
			Console.Error.WriteLine(inline);
			inputs = inline.Split(' ');
			int N = int.Parse(inputs[0]); // total number of players (2 to 4).
			int P = int.Parse(inputs[1]); // your player number (0 to 3).

			if (b == null)
			{
				b = new SilverBoard(N, P);
			}
			else
			{
				b.Update(N, P);
			}

			b.GetCycles();

			Console.WriteLine(b.GetMoveForMe()); // A single line with UP, DOWN, LEFT or RIGHT
		}
	}
}