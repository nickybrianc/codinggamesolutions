using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

static class Logger{
    public static void Log(string methodName, string message){
        Console.Error.WriteLine(
            string.Format(
                "[{0}: {1}]",
                methodName,
                message));
    }
}
static class Moves{
    public static string Move(int x, int y){
        return
            string.Format(
                "{0} {1}",
                x, y).ToString();
    }
}
class Point{
    public int X{get;set;}
    public int Y{get;set;}
    public void Update(int x, int y){
        X = x; Y = y;
    }
    public int Distance(Point q){
        return 
            (int)Math.Sqrt(
                (X - q.X) * (X - q.X) +
                (Y - q.Y) * (Y - q.Y));
    }
}
class Drone : Point{
}
class Player{
    public int ID {get; set;}
    public int NumDrones {get;}
    public List<Drone> Drones;
    
    internal Board _parent;
    public Player(int ID, int numDrones, Board parent){
        Drones = new List<Drone>();
        
        this.ID = ID;
        NumDrones = numDrones;
        _parent = parent;
        
        for(int i=0;i<NumDrones;i++){
            Drones.Add(new Drone());
        }
    }
    public virtual string GetDroneMove(int DroneId){
        var d = Drones[DroneId];
        var zs = _parent.Zones;
        
        return 
            zs
                .OrderBy(z => Math.Sqrt((z.X - d.X)*(z.X - d.X) + (z.Y - d.Y)*(z.Y - d.Y)))
                .Select(
                    z =>
                        string.Format(
                            "{0} {1}",
                            z.X, z.Y).ToString())
				.First();
    }
}
class Zone : Point{
    public int Controller {get;set;}
    public int ID {get;set;}
    public static int RADIUS = 100;
}
class Board{
    public List<Player> Players {get;}
    public List<Zone> Zones {get;}
    public int MyID;
    public Player MyPlayer{
        get{
            return Players[MyID];
        }
    }
    public Board(int numPlayers, int numZones, int MyID, int numDrones){
        Players = new List<Player>();
        Zones = new List<Zone>();
        
        this.MyID = MyID;
        
        InitPlayers(numPlayers, numDrones);
        InitZones(numZones);
    }
    public virtual void InitPlayers(int P, int D){
        for(int i=0;i<P;i++){
            Players.Add(new Player(i,D, this));
        }
    }
    public virtual void InitZones(int Z){
        string[] inputs;
        for (int i = 0; i < Z; i++)
        {
            inputs = Console.ReadLine().Split(' ');
            int X = int.Parse(inputs[0]); // corresponds to the position of the center of a zone. A zone is a circle with a radius of 100 units.
            int Y = int.Parse(inputs[1]);
            var z = new Zone();
            z.Update(X,Y);
            z.ID = i;
            Zones.Add(z);
        }
    }
    public virtual void UpdateZoneControl(){
        for (int i = 0; i < Zones.Count(); i++)
        {
            int TID = int.Parse(Console.ReadLine()); // ID of the team controlling the zone (0, 1, 2, or 3) or -1 if it is not controlled. The zones are given in the same order as in the initialization.
            Zones[i].Controller = TID;
        }
    }
    public virtual void UpdateDrones(){
        string[] inputs;
        for (int i = 0; i < Players.Count(); i++)
        {
            for (int j = 0; j < Players[i].Drones.Count(); j++)
            {
                inputs = Console.ReadLine().Split(' ');
                int DX = int.Parse(inputs[0]); // The first D lines contain the coordinates of drones of a player with the ID 0, the following D lines those of the drones of player 1, and thus it continues until the last player.
                int DY = int.Parse(inputs[1]);
                Players[i].Drones[j].Update(DX,DY);
            }
        }
    }
}
class BronzeDrone : Drone{
    int OwnerId;
    public int ID;
    public BronzeDrone(Drone d, int id){
        this.X = d.X;
        this.Y = d.Y;
        OwnerId = id;
    }
}
class BronzeBoard : Board{
    public BronzeBoard(int numPlayers, int numZones, int MyID, int numDrones) : base(numPlayers, numZones, MyID, numDrones) 
    { 
        foreach(var p in Players)
        {
            for(int i=0;i<p.Drones.Count();i++)
            {
                p.Drones[i] = new BronzeDrone(p.Drones[i], p.ID);
                (p.Drones[i] as BronzeDrone).ID = i;
            }    
        }
    }
    public override void InitPlayers(int P, int D){
        for(int i=0;i<P;i++){
            Players.Add(new BronzePlayer(i,D, this));
        }
    }
    public virtual int DronesAt(int x, int y, int id){
        return 
            Players[id]
                .Drones
                /*
                .Select(p => p.Drones.Where(d => d.X == x && d.Y == y))
                .Select(dList => dList.Count())
                .Sum();
                */
                .Where(d => d.X == x && d.Y == y)
                .Count();
                
    }
}
class BronzePlayer : Player{
    public BronzePlayer(int i, int d, Board b) : base(i,d,b) { }
    public virtual int DronesAt(int x, int y){
        var bb = _parent as BronzeBoard;
        
        var ret = bb.DronesAt(x,y, ID);

        Logger.Log(
            "BronzePlayer.DronesAt()",
            string.Format(
                "Zone {0},{1}: {2}", x,y,ret));
        return ret;
    }
    public override string GetDroneMove(int DroneId){
        var d = Drones[DroneId];
        var zs = _parent.Zones;
        
        if(zs.Any(z => z.X == d.X && z.Y == d.Y)){
            var stay = zs.First(z => z.X == d.X && z.Y == d.Y);
            return
                Moves.Move(stay.X,stay.Y);
        }
        
        return 
            zs
                .OrderBy(z => Math.Sqrt((z.X - d.X)*(z.X - d.X) + (z.Y - d.Y)*(z.Y - d.Y)))
                .Where(z => DronesAt(z.X,z.Y) < 3)
                .Select(
                    z =>
                        string.Format(
                            "{0} {1}",
                            z.X, z.Y).ToString())
                .DefaultIfEmpty("0 0")
				.First();      
    }
}
class BronzePlayer2 : BronzePlayer{
    private List<string> _turnMoves;
    
    public BronzePlayer2(int i, int d, Board b) : base(i,d,b) { } 
    
    public virtual List<string> GetDroneMoves()
	{
		Logger.Log("BronzePlayer2.GetDroneMoves()", "Entering ->");
		var ret = new string[NumDrones];

		//Strategty:
		//  1) For every zone, guarentee that it can be defended.
		//  2) Send drones to
		var bb2 = _parent as BronzeBoard2;
		foreach (var z in bb2.Zones.OrderBy(z => bb2.HighestEnemyCount(z)))
		{
			int myCount = bb2.FriendliesAt(z);
			int unused = ret.Where(r => string.IsNullOrEmpty(r)).Count();
			int highestEnemyCount = bb2.HighestEnemyCount(z);
            
            Logger.Log(
                "BronzePlayer2.GetDroneMove()", 
                string.Format(
                    "Zone {0}: {1},{2},{3},{4}", 
                    z.ID,z.Controller, myCount, unused, highestEnemyCount));
            
			//My Zone
			if (z.Controller == ID)
			{
			    Console.Error.WriteLine("DEFENDING!");
				int i = 0;
				//Keep Guards Stationed
				for (; i < highestEnemyCount + 1; i++)
				{
					for (int j = 0; j < NumDrones && i < highestEnemyCount + 1; j++)
					{
						if (Drones[j].Distance(z) <= 100)
						{
							ret[j] = Moves.Move(z.X, z.Y) + " Zone!";
							i++;
						}
					}
				}
			}
			else if(unused > highestEnemyCount)
			{
				int i = 0;
				Console.Error.WriteLine(unused + "," + highestEnemyCount);
				for (; i < highestEnemyCount + 1; i++)
				{
					var drones = Drones.OrderBy(d => d.Distance(z));
					foreach (BronzeDrone d in drones)
					{
						if (string.IsNullOrWhiteSpace(ret[d.ID]))
						{
							ret[d.ID] = Moves.Move(z.X, z.Y);
						}
						else{
						    Logger.Log("BronzePlayer2().GetDroneMoves()", 
						        string.Format(
						            "Apparently {0} is busy with [{1}]",
						            d.ID, ret[d.ID]));
						}
					}
				}
			}
		}
        
		for (int i = 0; i < NumDrones; i++)
		{
			Logger.Log(
				"BronzePlayer2.GetDroneMoves()",
				"Chosen: " + ret[i]);
			if (string.IsNullOrEmpty(ret[i]))
			{
				ret[i] = base.GetDroneMove(i);
			}
		}

		return ret.ToList();
	}
    public virtual void Update(){
        _turnMoves = null;
    }
    public override string GetDroneMove(int DroneId){
        if(_turnMoves == null){
            _turnMoves = GetDroneMoves();
        }
        return _turnMoves[DroneId];
    }
}
class BronzeBoard2 : BronzeBoard{
    public BronzeBoard2(int numPlayers, int numZones, int MyID, int numDrones) : base(numPlayers, numZones, MyID, numDrones) 
    { }
    public override void InitPlayers(int P, int D){
        for(int i=0;i<P;i++){
            Players.Add(new BronzePlayer2(i,D,this));
        }
    }
    public virtual void UpdatePlayers(){
        foreach(var p in Players){
            var bp2 = p as BronzePlayer2;
            
            bp2.Update();
        }
    }
    public virtual int FriendliesAt(Zone z){
        return 
            MyPlayer.Drones.Where(d => d.Distance(z) <= 100).Count();
    }
    public virtual int HighestEnemyCount(Zone z){
        int ret = 0;
        int zid = 0;
        var search = Players.Where(p => p.ID != MyPlayer.ID);
        foreach(var p in search){
            int t = p.Drones.Where(d => d.Distance(z) <= 100).Count();
            if(t > ret){
                ret = t;
                zid = p.ID;
            }
        }
        /*
        Logger.Log("BronzeBoard2.HighestEnemyCount()", 
            string.Format(
                "Zone {0},{1}, {2}",
                z.X,
                z.Y,
                ret));
        */
        return ret;
    }
}
class Start
{
    static void Main(string[] args)
    {
        string[] inputs;
        inputs = Console.ReadLine().Split(' ');
        int P = int.Parse(inputs[0]); // number of players in the game (2 to 4 players)
        int ID = int.Parse(inputs[1]); // ID of your player (0, 1, 2, or 3)
        int D = int.Parse(inputs[2]); // number of drones in each team (3 to 11)
        int Z = int.Parse(inputs[3]); // number of zones on the map (4 to 8)
        
        BronzeBoard2 b = new BronzeBoard2(P,Z,ID,D);
    
        // game loop
        while (true)
        {
            b.UpdateZoneControl();
            
            b.UpdateDrones();
            
            b.UpdatePlayers();
            
            for (int i = 0; i < D; i++)
            {
                Console.WriteLine(b.MyPlayer.GetDroneMove(i));
            }
        }
    }
}